﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace Lab3
{
    class MockHandler
    {
        public static void ShowMock(ClientsAndObjectsTest clientsAndObjectsTest)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("All clients: ");
            Console.ResetColor();
            foreach( string s in (clientsAndObjectsTest.Clients.Select((c) => c.ToString()).ToList()))
                Console.WriteLine(s);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nAll objects");
            Console.ResetColor();
            foreach (string s in (clientsAndObjectsTest.EstateObjects.Select((e) => e.ToString()).ToList()))
                Console.WriteLine(s);
        }
        public static void GroupObjectsByOwner(ClientsAndObjectsTest clientsAndObjectsTest)
        {
            var objectGroups = from estateObject in clientsAndObjectsTest.EstateObjects
                               group estateObject by estateObject.OwnerId;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nObject by owners:");
            Console.ResetColor();
            foreach (IGrouping<int, EstateObject> g in objectGroups)
            {
                Console.WriteLine("Owner with ID: " + g.Key);
                foreach (var t in g)
                    Console.WriteLine("  " + t.ToString());
            }
        }
        public static void OrderObjectsByCity(ClientsAndObjectsTest clientsAndObjectsTest, bool orderWay)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nObjects ordered by city:");
            Console.ResetColor();
            if( orderWay == true )
            {
                var orderedObjects = clientsAndObjectsTest.EstateObjects.OrderBy(o => o.City);
                foreach(var v in orderedObjects)
                    Console.WriteLine(v.ToString());
            }
            else
            {
                var orderedObjects = clientsAndObjectsTest.EstateObjects.OrderByDescending(o => o.City);
                foreach (var v in orderedObjects)
                    Console.WriteLine(v.ToString());
            }
        }
        public static void ClientsAndCities(ClientsAndObjectsTest clientsAndObjectsTest)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nClients and cities:");
            Console.ResetColor();
            var result = from c in clientsAndObjectsTest.Clients
                         join o in clientsAndObjectsTest.EstateObjects on c.Id equals o.OwnerId
                         select new { OwnerName = c.Name, OwnerSurname = c.Surname, City = o.City};
            foreach (var item in result)
                Console.WriteLine($"{item.OwnerName} {item.OwnerSurname} has object in {item.City}");
        }
        public static void CitiesAndAvaragePrice(ClientsAndObjectsTest clientsAndObjectsTest)
        {
            var result = clientsAndObjectsTest.EstateObjects.GroupBy(e => e.City)
                        .Select(g => new { Name = g.Key, Avrg = g.Average(e => e.PricePerDay) });
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nCities and avarage prices: ");
            Console.ResetColor();
            foreach (var item in result)
                Console.WriteLine($"{item.Name}  ~{item.Avrg}");
        }
        public static void CheckCity(ClientsAndObjectsTest clientsAndObjectsTest, string cityName)
        {
            bool result = clientsAndObjectsTest.EstateObjects.Any(o => o.City.Equals(cityName));
            Console.ForegroundColor = ConsoleColor.Blue;
            if (result == false)
                Console.WriteLine("\nThere isn`t any proposition from " + cityName);
            else
                Console.WriteLine("\nWe have any object in " + cityName);
            Console.ResetColor();
        }
        public static void CountSumForObjects(ClientsAndObjectsTest clientsAndObjectsTest)
        {
            int sum = clientsAndObjectsTest.EstateObjects.Sum(o => o.PricePerDay);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"\nAll objects cost: {sum}");
            Console.ResetColor();
        }
        public static void MinAndMaxPriceInCity(ClientsAndObjectsTest clientsAndObjectsTest, string cityName)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            if(!clientsAndObjectsTest.EstateObjects.Any(o => o.City.Equals(cityName)))
            {
                Console.WriteLine("\nThere isn`t such city");
                Console.ResetColor();
                return;
            }
            int min = clientsAndObjectsTest.EstateObjects.Where(o => o.City.Equals(cityName)).Min(o => o.PricePerDay);
            int max = clientsAndObjectsTest.EstateObjects.Max(o => o.PricePerDay);
            Console.WriteLine($"\nObjects prices are in such range: {min} - {max}");
            Console.ResetColor();
        }
        public static void ClientByCriteria(ClientsAndObjectsTest clientsAndObjectsTest, Func<Client, bool> criteria)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nClients by criteria: ");
            Console.ResetColor();
            var result = clientsAndObjectsTest.Clients.Where(criteria);
            foreach (string s in (result.Select((c) => c.ToString()).ToList()))
                Console.WriteLine(s);
        }
        public static void TopExpensive(ClientsAndObjectsTest clientsAndObjectsTest, int places)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"\nTop {places} expensive objects: ");
            Console.ResetColor();
            var result = clientsAndObjectsTest.EstateObjects.OrderByDescending(o => o.PricePerDay).Take(places);
            foreach (string s in (result.Select((c) => c.ToString()).ToList()))
                Console.WriteLine(s);
        }
    }
}

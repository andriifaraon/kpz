﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class EstateObject: IComparable<EstateObject>
    {
        public int Id { get; private set; }
        public int OwnerId { get; private set; }
        public EstateType TypeOfEstate { get; private set; }
        public string City { get; private set; }
        public int PricePerDay { get; private set; }
        public EstateObject( int id, int ownerId, EstateType estateType, string city, int pricePerDay )
        {
            Id = id;
            OwnerId = ownerId;
            TypeOfEstate = estateType;
            City = city;
            PricePerDay = pricePerDay;
        }
        public override string ToString()
        {
            return string.Format($"{Id} {OwnerId} {TypeOfEstate} {City} {PricePerDay}");
        }
        public override bool Equals(object obj)
        {
            EstateObject estateObject = obj as EstateObject;
            if (estateObject == null)
                return false;
            if (estateObject.Id == Id)
                return true;
            return false;
        }

        public int CompareTo(EstateObject other)
        {
            if (other.Id == Id)
                return 0;
            else if (other.Id < Id)
                return 1;
            else
                return -1;
        }
    }
    enum EstateType
    {
        Store,
        Flat,
        House,
        Factory
    }
}

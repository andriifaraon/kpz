﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class EstateAgent: IComparable<EstateAgent>
    {
        public int Id { get; private set; }
        public int Experience { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public EstateAgent(int id, string name, string surname, int experience)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Experience = experience;
        }
        public override string ToString()
        {
            return string.Format($"{Id} {Name} {Surname} {Experience}");
        }
        public override bool Equals(object obj)
        {
            EstateAgent estateAgent = obj as EstateAgent;
            if (estateAgent == null)
                return false;
            if (estateAgent.Id == Id)
                return true;
            return false;
        }

        public int CompareTo(EstateAgent other)
        {
            if (other.Id == Id)
                return 0;
            else if (other.Id < Id)
                return 1;
            else
                return -1;
        }
    }
}

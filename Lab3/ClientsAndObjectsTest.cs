﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class ClientsAndObjectsTest
    {
        public List<Client> Clients
        {
            get
            {
                return new List<Client>
                {
                    new Client(1, "Danylo", "Petrov"),
                    new Client(2, "Vasyl", "Honta"),
                    new Client(3, "Natalia", "Ruda"),
                    new Client(4, "Andrii", "Zaliznyak"),
                    new Client(5, "Anastasiia", "Lisovska"),
                    new Client(6, "Andrii", "Kalymon")
                };
            }
        }
        public List<EstateObject> EstateObjects
        {
            get
            {
                return new List<EstateObject>
                {
                    new EstateObject(1, 3, EstateType.House, "Kyiv", 230),
                    new EstateObject(2, 4, EstateType.Flat, "Lviv", 270),
                    new EstateObject(3, 5, EstateType.House, "Kyiv", 230),
                    new EstateObject(4, 3, EstateType.Flat, "Odessa", 280),
                    new EstateObject(5, 3, EstateType.Factory, "Kyiv", 330),
                    new EstateObject(6, 1, EstateType.Store, "Kyiv", 230),
                    new EstateObject(7, 1, EstateType.House, "Komarno", 400),
                    new EstateObject(8, 5, EstateType.House, "Kyiv", 230),
                    new EstateObject(9, 2, EstateType.Flat, "Horodok", 480),
                    new EstateObject(10, 3, EstateType.Factory, "Kyiv", 330),
                    new EstateObject(11, 6, EstateType.House, "Odessa", 530),
                    new EstateObject(12, 6, EstateType.Flat, "Horodok", 480),
                    new EstateObject(13, 3, EstateType.Factory, "Herson", 330),
                    new EstateObject(14, 3, EstateType.Store, "Odessa", 380),
                    new EstateObject(15, 3, EstateType.Factory, "Kyiv", 470),
                };
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public class Contract: IComparable<Contract>
    {
        public int Id { get; private set; }
        public int ClientId { get; private set; }
        public int EstateAgentId { get; private set; }
        public int EstateObjectId { get; private set; }
        public int DayDuration { get; private set; }
        public DateTime RentStart { get; private set; }
        public int Price { get; private set; }
        public Contract(int id, int clientId, int estateAgentId, int estateObjectId, int dayDuration, DateTime rentStart, int pricePerDay )
        {
            Id = id;
            ClientId = clientId;
            EstateAgentId = estateAgentId;
            EstateObjectId = estateObjectId;
            DayDuration = dayDuration;
            RentStart = rentStart;
            Price = dayDuration * pricePerDay;
        }
        public override string ToString()
        {
            return string.Format($"{Id} {ClientId} {EstateAgentId} {EstateObjectId} {DayDuration} {RentStart} {Price}");
        }
        public override bool Equals(object obj)
        {
            Contract contract = obj as Contract;
            if (contract == null)
                return false;
            if (contract.Id == Id)
                return true;
            return false;
        }

        public int CompareTo(Contract other)
        {
            if (other.Id == Id)
                return 0;
            else if (other.Id < Id)
                return 1;
            else
                return -1;
        }
    }
    public static class IEnumerableExtension
    {
        public static Dictionary<int, int> ContractsByObjects( this SortedSet<Contract> contracts )
        {
            var contractGroups = from contract in contracts
                               group contract by contract.EstateObjectId into g
                               select new { Id = g.Key, Count = g.Count() };
            return contractGroups.ToDictionary(k => k.Id, v => v.Count);
        }
    }
}

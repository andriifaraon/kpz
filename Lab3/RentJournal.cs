﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Lab3
{
    class RentJournal
    {
        private SortedSet<Client> clients = new SortedSet<Client>();
        private SortedSet<EstateAgent> estateAgents = new SortedSet<EstateAgent>();
        private SortedSet<EstateObject> estateObjects = new SortedSet<EstateObject>();
        private SortedSet<Contract> contracts = new SortedSet<Contract>();
        private Dictionary<int, List<Contract>> clientsWithContracts = new Dictionary<int, List<Contract>>();
        public void AddClient(Client client) => clients.Add(client);
        public void AddClient(params Client[] _clients)
        {
            foreach (Client client in _clients)
                clients.Add(client);
        }
        public void AddEstateAgent(EstateAgent estateAgent) => estateAgents.Add(estateAgent);
        public void AddEstateAgent(params EstateAgent[] _agents)
        {
            foreach (EstateAgent agent in _agents)
                estateAgents.Add(agent);
        }
        public void AddEsatateObject(EstateObject estateObject) => estateObjects.Add(estateObject);
        public void AddEstateObject(params EstateObject[] _objects)
        {
            foreach (EstateObject estateObject in _objects)
                estateObjects.Add(estateObject);
        }
        public void FormContract( Client _client, EstateAgent _estateAgent, EstateObject _estateObject, DateTime rentStart, int dayDuration)
        {
            Client client = clients.FirstOrDefault((c) => c.Equals(_client));
            EstateAgent estateAgent = estateAgents.FirstOrDefault((a) => a.Equals(_estateAgent));
            EstateObject estateObject = estateObjects.FirstOrDefault((o) => o.Equals(_estateObject));
            if( client != null && estateObject != null && estateAgent != null)
            {
                Contract contract =
                    new Contract
                    (contracts.Count + 1, client.Id, estateAgent.Id, estateObject.Id, dayDuration, rentStart, estateObject.PricePerDay);
                contracts.Add(contract);
                RefreshJournal();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is no such client, agent or object");
                Console.ResetColor();
            }
        }
        private void RefreshJournal()
        {
            clientsWithContracts.Clear();
            foreach( Client client in clients )
            {
                int clientId = client.Id;
                var clientContracts = contracts.Where((c) => c.ClientId == clientId).ToList();
                clientsWithContracts.Add(clientId, clientContracts);
            }
        }
        public string GetClientsWithContracts()
        {
            Console.WriteLine("Every client has such contracts:");
            StringBuilder stringBuilder = new StringBuilder("");
            int[] keys = clientsWithContracts.Keys.ToArray();
            for( int i=0; i<keys.Length; i++)
            {
                stringBuilder.Append("Id = " + keys[i] + "\n");
                List<Contract> clientContracts = clientsWithContracts[keys[i]];
                foreach (Contract contract in clientContracts)
                    stringBuilder.Append(contract + "\n");
            }
            return stringBuilder.ToString();
        }
        public string GetAvailableCities()
        {
            Console.WriteLine("Available cities:");
            StringBuilder stringBuilder = new StringBuilder("");
            var cities = estateObjects.Select((o) => new { o.City }).Distinct().ToList();
            foreach (var city in cities)
                stringBuilder.Append(city.City + " ");
            return stringBuilder.ToString() + "\n";
        }
        public string GetClientsSortedByName()
        {
            Console.WriteLine("Sorted clients by name:");
            StringBuilder stringBuilder = new StringBuilder("");
            List<Client> sortedClients = clients.ToList();
            sortedClients.Sort(new ClientsByNameComparer());
            foreach (var client in sortedClients)
                stringBuilder.Append(client + "\n");
            return stringBuilder.ToString();
        }
        public string GetObjectsByContracts()
        {
            Console.WriteLine("Estate objects and count of contracts:");
            Dictionary<int, int> objectsByContracts = contracts.ContractsByObjects();
            StringBuilder stringBuilder = new StringBuilder("");
            foreach (KeyValuePair<int, int> keyValue in objectsByContracts)
            {
                stringBuilder.Append(keyValue.Key + " - " + keyValue.Value + " times\n");
            }
            return stringBuilder.ToString();
        }
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder("We have clients:\n");
            foreach (Client client in clients)
                stringBuilder.Append(client + "\n");
            stringBuilder.Append("\nWe have agents:\n");
            foreach (EstateAgent estateAgent in estateAgents)
                stringBuilder.Append(estateAgent + "\n");
            stringBuilder.Append("\nWe have objects:\n");
            foreach (EstateObject estateObject in estateObjects)
                stringBuilder.Append(estateObject + "\n");
            stringBuilder.Append("\nWe have contracts:\n");
            foreach (Contract contract in contracts)
                stringBuilder.Append(contract + "\n");
            return stringBuilder.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Enumerable.Range(0, 10).Select((x) => x * x);
            foreach (var v in result)
                Console.Write(v + " ");
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine("TASK #1");
            //Console.ResetColor();
            //Client c1 = new Client(1, "Andre", "Kalymon");
            //Client c2 = new Client(2, "Olia", "Kalymon");
            //Client c3 = new Client(3, "Danylo", "Kovtun");
            //EstateAgent a1 = new EstateAgent(1, "Oleh", "Vynnyk", 3);
            //EstateAgent a2 = new EstateAgent(1, "Vika", "Vynnyk", 3);
            //EstateObject o1 = new EstateObject(4, c1.Id, EstateType.Flat, "Lviv", 350);
            //EstateObject o2 = new EstateObject(5, c2.Id, EstateType.Flat, "Komarno", 250);
            //EstateObject o3 = new EstateObject(6, c2.Id, EstateType.Flat, "Komarno", 150);
            //RentJournal rentJournal = new RentJournal();
            //rentJournal.AddClient(c1, c2, c3);
            //rentJournal.AddEstateObject(o1, o2, o3);
            //rentJournal.AddEstateAgent(a1, a2);
            //rentJournal.FormContract(c1, a1, o1, DateTime.Now, 4);
            //rentJournal.FormContract(c1, a1, o2, DateTime.Now, 5);
            //rentJournal.FormContract(c2, a2, o1, DateTime.Now, 7);
            //rentJournal.FormContract(c1, a1, o3, DateTime.Now, 4);
            //rentJournal.FormContract(c1, a1, o1, DateTime.Now, 4);
            //Console.WriteLine(rentJournal.GetClientsWithContracts());
            //Console.WriteLine(rentJournal.GetAvailableCities());
            //Console.WriteLine(rentJournal.GetClientsSortedByName());
            //Console.WriteLine(rentJournal.GetObjectsByContracts());

            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine("TASK #2");
            //Console.ResetColor();
            //ClientsAndObjectsTest clientsAndObjectsTest = new ClientsAndObjectsTest();
            //MockHandler.ShowMock(clientsAndObjectsTest);
            //MockHandler.GroupObjectsByOwner(clientsAndObjectsTest);
            //MockHandler.OrderObjectsByCity(clientsAndObjectsTest, true);
            //MockHandler.ClientsAndCities(clientsAndObjectsTest);
            //MockHandler.CitiesAndAvaragePrice(clientsAndObjectsTest);
            //MockHandler.CheckCity(clientsAndObjectsTest, "Kyiv");
            //MockHandler.CountSumForObjects(clientsAndObjectsTest);
            //MockHandler.MinAndMaxPriceInCity(clientsAndObjectsTest, "Odessa");
            //MockHandler.ClientByCriteria(clientsAndObjectsTest, (c) => c.Name.Equals("Andrii"));
            //MockHandler.TopExpensive(clientsAndObjectsTest, 3);
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public enum OperationType
    {
        Plus,
        Minus,
        Multiply,
        Devide
    }
    public class PrintEventArgs: EventArgs
    {
        public OperationType Operation { get; set; }
        public string Message { get; set; }
        public double Result { get; set; }
    }
}

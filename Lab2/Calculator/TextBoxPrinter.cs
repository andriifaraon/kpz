﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Calculator
{
    public class TextBoxPrinter
    {
        private TextBox textBox;
        public TextBoxPrinter(TextBox textBox)
        {
            this.textBox = textBox;
        }
        public void PrintOnTextBox(PrintEventArgs printEventArgs)
        {
            StringBuilder textToWrite = new StringBuilder(textBox.Text);
            textToWrite.Append(printEventArgs.Message + printEventArgs.Operation + " is " + printEventArgs.Result + "\n");
            textBox.Text = textToWrite.ToString();
        }
        public void PrintOnTextBox(ErrorEventArgs errorEventArgs)
        {
            StringBuilder textToWrite = new StringBuilder(textBox.Text);
            textToWrite.Append(errorEventArgs.Message + " with code " + errorEventArgs.ErrorCode + "\n");
            textBox.Text = textToWrite.ToString();
        }
    }
}

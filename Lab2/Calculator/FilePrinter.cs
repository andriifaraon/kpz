﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Calculator
{
    public class FilePrinter
    {
        private StreamWriter streamToWrite;
        public FilePrinter(string fileName)
        {
            streamToWrite = new StreamWriter(fileName);
            streamToWrite.WriteLine("");
            streamToWrite.Close();
            streamToWrite = new StreamWriter(fileName, true);
        }
        public void PrintOnFile(PrintEventArgs printEventArgs)
        {
            streamToWrite.WriteLine(printEventArgs.Message + printEventArgs.Operation + " is " + printEventArgs.Result);
        }
        public void PrintOnFile(ErrorEventArgs errorEventArgs)
        {
            streamToWrite.WriteLine(errorEventArgs.Message + " with code " + errorEventArgs.ErrorCode);
        }
        public void CloseStream()
        {
            streamToWrite.Close();
        }
    }
}

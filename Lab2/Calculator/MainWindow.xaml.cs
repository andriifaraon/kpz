﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MyCalculator calculator;
        private TextBoxPrinter textBoxPrinter;
        private FilePrinter filePrinter;
        public MainWindow()
        {
            InitializeComponent();
            calculator = new MyCalculator();
            textBoxPrinter = new TextBoxPrinter(TextLog);
            filePrinter = new FilePrinter("calculator_log.txt");
            calculator.SubscribePrintEvent(textBoxPrinter.PrintOnTextBox);
            calculator.SubscribePrintEvent(filePrinter.PrintOnFile);
            calculator.SubscribeErrorEvent(textBoxPrinter.PrintOnTextBox);
            calculator.SubscribeErrorEvent(filePrinter.PrintOnFile);
            calculator.SubscribeErrorEvent(
                (args) => MessageBox.Show(args.Message + " with code " + args.ErrorCode, "Error", MessageBoxButton.OK,
                MessageBoxImage.Error));
        }
        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            calculator.Plus(FirstOperand.Text, SecondOperand.Text);
        }
        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            calculator.Minus(FirstOperand.Text, SecondOperand.Text);
        }
        private void MultiplyButton_Click(object sender, RoutedEventArgs e)
        {
            calculator.Multiply(FirstOperand.Text, SecondOperand.Text);
        }
        private void DivideButton_Click(object sender, RoutedEventArgs e)
        {
            calculator.Devide(FirstOperand.Text, SecondOperand.Text);
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            filePrinter.CloseStream();
        }
    }
}

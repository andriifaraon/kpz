﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class ErrorEventArgs
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
    }
}

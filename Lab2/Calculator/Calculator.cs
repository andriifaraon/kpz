﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Calculator
{
    public class MyCalculator
    {
        public event Action<PrintEventArgs> PrintEvent;
        public event Action<ErrorEventArgs> ErrorEvent;
        public void SubscribePrintEvent(Action<PrintEventArgs> printEvent)
        {
            PrintEvent += printEvent;
        }
        public void SubscribeErrorEvent(Action<ErrorEventArgs> errorEvent)
        {
            ErrorEvent += errorEvent;
        }
        public void Plus(string a, string b)
        {
            double firstOperand;
            double secondOperand;
            if(double.TryParse(a, out firstOperand) && double.TryParse(b, out secondOperand))
            {
                PrintEventArgs printEventArgs = new PrintEventArgs()
                { Message = "Result of operation ", Operation = OperationType.Plus, Result = firstOperand + secondOperand };
                PrintEvent?.Invoke(printEventArgs);
            }
            else
            {
                ErrorEventArgs errorEventArgs = new ErrorEventArgs() { ErrorCode = 1, Message = "Converting error" };
                ErrorEvent?.Invoke(errorEventArgs);
            }
        }
        public void Minus(string a, string b)
        {
            double firstOperand;
            double secondOperand;
            if (double.TryParse(a, out firstOperand) && double.TryParse(b, out secondOperand))
            {
                PrintEventArgs printEventArgs = new PrintEventArgs()
                { Message = "Result of operation ", Operation = OperationType.Minus, Result = firstOperand - secondOperand };
                PrintEvent?.Invoke(printEventArgs);
            }
            else
            {
                ErrorEventArgs errorEventArgs = new ErrorEventArgs() { ErrorCode = 1, Message = "Converting error" };
                ErrorEvent?.Invoke(errorEventArgs);
            }
        }
        public void Multiply(string a, string b)
        {
            double firstOperand;
            double secondOperand;
            if (double.TryParse(a, out firstOperand) && double.TryParse(b, out secondOperand))
            {
                PrintEventArgs printEventArgs = new PrintEventArgs()
                { Message = "Result of operation ", Operation = OperationType.Multiply, Result = firstOperand * secondOperand };
                PrintEvent?.Invoke(printEventArgs);
            }
            else
            {
                ErrorEventArgs errorEventArgs = new ErrorEventArgs() { ErrorCode = 1, Message = "Converting error" };
                ErrorEvent?.Invoke(errorEventArgs);
            }
        }
        public void Devide(string a, string b)
        {
            double firstOperand;
            double secondOperand;
            if (double.TryParse(a, out firstOperand) && double.TryParse(b, out secondOperand))
            {
                if (secondOperand.Equals(0))
                {
                    ErrorEventArgs errorEventArgs = new ErrorEventArgs() { ErrorCode = 2, Message = "Deviding by zero error" };
                    ErrorEvent?.Invoke(errorEventArgs);
                }
                else
                {
                    PrintEventArgs printEventArgs = new PrintEventArgs()
                    { Message = "Result of operation ", Operation = OperationType.Devide, Result = firstOperand / secondOperand };
                    PrintEvent?.Invoke(printEventArgs);
                }
            }
            else
            {
                ErrorEventArgs errorEventArgs = new ErrorEventArgs() { ErrorCode = 1, Message = "Converting error" };
                ErrorEvent?.Invoke(errorEventArgs);
            }
        }
    }
}

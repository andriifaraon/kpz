﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Kalymon.Andrii.RobotChallange
{
    public class GenerationHelper
    {
        public static MoveCommand GenerateMoveCommandToAttack
            ( int roundNumber, int numberOfMyRobots, int roundNumberToAttack, int numberOfMyRobotsToAttack, int profit, 
            Robot.Common.Robot movingRobot, Robot.Common.Robot robotToAttack )
        {
            if (roundNumber > roundNumberToAttack  && numberOfMyRobots > numberOfMyRobotsToAttack)
            {
                if (robotToAttack != null)
                {
                    if ((ValidationHelper.IsNormalToAttack(robotToAttack, movingRobot, profit)))
                    {
                        if (ValidationHelper.IsHasEnergyToGo(movingRobot, robotToAttack.Position, 1))
                            return new MoveCommand() { NewPosition = robotToAttack.Position };
                    }
                }
            }
            return null;
        }
        public static CreateNewRobotCommand GenerateCreateNewRobotCommand
            (Robot.Common.Robot movingRobot, int energyToCreate, Map map, IList<Robot.Common.Robot> robots, 
            int roundNumber, int roundNumberToCreate, int numberOfMyRobots)
        {
            if ((movingRobot.Energy > energyToCreate) && (robots.Count < map.Stations.Count)
                && (roundNumber <= roundNumberToCreate) && numberOfMyRobots < 100)
            {
                return new CreateNewRobotCommand();
            }
            return null;
        }
    }
}

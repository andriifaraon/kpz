﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Kalymon.Andrii.RobotChallange
{
    public class CalculationHelper
    {
        public static int CalculateEnergyBetweenCells(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        public static List<Position> CalculateCellsAroundStation( Position position, Map map )
        {
            int X = position.X;
            int Y = position.Y;
            List<Position> positions = new List<Position>();
            positions.Add(position);         
            for( int i=X-2; i<=X+2; i++)
            {
                Position newPosition = new Position(i, Y);
                if (map.IsValid(newPosition) && newPosition != position)
                    positions.Add(newPosition);
            }
            for( int j=Y-2; j<=Y+2; j++)
            {
                Position newPosition = new Position(X, j);
                if (map.IsValid(newPosition) && newPosition!=position)
                    positions.Add(newPosition);
            }
            for( int i=X-1; i<=X+1; i+=2)
            {
                for( int j=Y-1; j<=Y+1; j+=2)
                {
                    Position newPosition = new Position(i, j);
                    if (map.IsValid(newPosition) && newPosition != position)
                        positions.Add(newPosition);
                }
            }
            return positions;
        }
        public static int CalculateNumberOfRobots(IList<Robot.Common.Robot> robots, string ownerName)
        {
            int res = 0;
            foreach (var robot in robots)
                if (robot.OwnerName == ownerName)
                    res++;
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
using System.Reflection;

namespace Kalymon.Andrii.RobotChallange
{
    public class KalymonAlgorithm : IRobotAlgorithm
    {
        private const int possibleRoundToCreateNewRobot = 45;
        public int RoundNumber { get; set; }
        public string Author => "Andriy KalymonR";
        public KalymonAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundNumber++;
        }
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            int numberOfMyRobots = CalculationHelper.CalculateNumberOfRobots(robots, Author);
            Position stationPosition = FindingHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            List<Robot.Common.Robot> enemies = FindingHelper.FindEnemies(robots, Author);
            Robot.Common.Robot robotToAttack = FindingHelper.FindMostProfitRobotToAttack(enemies, movingRobot);
            MoveCommand moveCommandInNormalCondition =
                GenerationHelper.GenerateMoveCommandToAttack(RoundNumber, numberOfMyRobots, 20, 30, 25, movingRobot, robotToAttack);
            MoveCommand moveCommandInExtremalCondition =
                GenerationHelper.GenerateMoveCommandToAttack(RoundNumber, numberOfMyRobots, 5, 15, 5, movingRobot, robotToAttack);
            CreateNewRobotCommand normalCreateNewRobotCommand =
                GenerationHelper.GenerateCreateNewRobotCommand
                (movingRobot, 380, map, robots, RoundNumber, possibleRoundToCreateNewRobot, numberOfMyRobots);
            EnergyStation energyStation = FindingHelper.FindStationByPosition(map, stationPosition);

            if (normalCreateNewRobotCommand != null)
                return normalCreateNewRobotCommand;

            if (moveCommandInNormalCondition != null)
                return moveCommandInNormalCondition;

            if (stationPosition == null && robotToAttack != null)
            {
                Position possiblePositionToAttackRobot =
                    FindingHelper.FindPossiblePositionToGo(movingRobot, robots, map, robotToAttack.Position, 1);
                if (possiblePositionToAttackRobot != null)
                    return new MoveCommand() { NewPosition = possiblePositionToAttackRobot };
            }
            if (stationPosition == null && robotToAttack == null)
                return new CollectEnergyCommand();
            if (stationPosition == movingRobot.Position)
            {
                if (energyStation != null)
                {
                    if (energyStation.Energy != 0)
                        return new CollectEnergyCommand();
                    else
                    {
                        if (moveCommandInExtremalCondition != null)
                            return moveCommandInExtremalCondition;
                    }
                }
            }
            else
            {
                Position possiblePositionToEnergyStation =
                    FindingHelper.FindPossiblePositionToGo(movingRobot, robots, map, stationPosition, 1);
                if (ValidationHelper.IsHasEnergyToGo(movingRobot, stationPosition, 1))
                    return new MoveCommand() { NewPosition = stationPosition };
                if (possiblePositionToEnergyStation != null)
                    return new MoveCommand() { NewPosition = possiblePositionToEnergyStation };
            }
            if (moveCommandInExtremalCondition != null)
                return moveCommandInExtremalCondition;
            return new CollectEnergyCommand();
        }
    }
}

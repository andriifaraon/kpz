﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Kalymon.Andrii.RobotChallange
{
    public class FindingHelper
    {
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (ValidationHelper.IsStationFree(station, movingRobot, robots, map))
                {
                    int d = CalculationHelper.CalculateEnergyBetweenCells(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            if( nearest != null )
            {
                Position result = null;
                List<Position> positions = CalculationHelper.CalculateCellsAroundStation(nearest.Position, map);
                int minDistanceAroundStation = int.MaxValue;
                foreach( Position p in positions )
                {
                    int d = CalculationHelper.CalculateEnergyBetweenCells(p, movingRobot.Position);
                    if (d < minDistanceAroundStation)
                    {
                        minDistanceAroundStation = d;
                        result = p;
                    }
                }
                return result;
            }
            return nearest == null ? null : nearest.Position;
        }
        public static Robot.Common.Robot FindMostProfitRobotToAttack(List<Robot.Common.Robot> enemies,
        Robot.Common.Robot movingRobot)
        {
            enemies.Sort(new EnemyComparer() { myRobot = movingRobot });
            enemies.Reverse();
            for( int i=0; i<enemies.Count; i++)
            {
                if (ValidationHelper.IsHasEnergyToGo(movingRobot, enemies[i].Position, 20 /*60*/))
                {
                    return enemies[i];
                }                
            }
            return null;
        }
        public static List<Robot.Common.Robot> FindEnemies(IList<Robot.Common.Robot> robots, string ownerName)
        {
            return robots.ToList().FindAll((r) => r.OwnerName != ownerName);
        }
        public static Position FindHalfPositionOnTheWay( Position a, Position b)
        {
            if ((a.X < b.X) && (a.Y < b.Y))
                return new Position(a.X + (b.X - a.X) / 2, a.Y + (b.Y - a.Y) / 2);
            else if ((a.X > b.X) && (a.Y > b.Y))
                return new Position(a.X - (a.X - b.X) / 2, a.Y - (a.Y - b.Y) / 2);
            else if ((a.X < b.X) && (a.Y > b.Y))
                return new Position(a.X + (b.X - a.X) / 2, a.Y - (a.Y - b.Y) / 2);
            else if ((a.X > b.X) && (a.Y < b.Y))
                return new Position(a.X - (a.X - b.X) / 2, a.Y + (b.Y - a.Y) / 2);
            else if (a.X == b.X)
            {
                if (a.Y > b.Y)
                    return new Position(a.X, a.Y - (a.Y - b.Y) / 2);
                else if (a.Y < b.Y)
                    return new Position(a.X, a.Y + (b.Y - a.Y) / 2);
            }
            else if (a.Y == b.Y)
            {
                if (a.X > b.X)
                    return new Position(a.X - (a.X - b.X) / 2, a.Y);
                else if (a.X < b.X)
                    return new Position(a.X + (b.X - a.X) / 2, a.Y);
            }
                return new Position(a.X, b.Y);
        }
        public static Position FindQuarterPositionOnTheWay( Position a, Position b)
        {
            Position halfPositionOnTheWay = FindHalfPositionOnTheWay(a, b);
            return FindHalfPositionOnTheWay(a, halfPositionOnTheWay);
        }
        public static Position FindPossiblePositionToGo(Robot.Common.Robot movingRobot, 
            IList<Robot.Common.Robot> robots, Map map, Position positionToGo, int energyToLeave)
        {
            Position halfPosition = FindHalfPositionOnTheWay(movingRobot.Position, positionToGo);
            bool isHalfPositionFreeFromRobots =
                ValidationHelper.IsCellFreeFromRobots(halfPosition, movingRobot, robots, map);
            bool isHasEnergyToGoToHalfPosition =
                ValidationHelper.IsHasEnergyToGo(movingRobot, halfPosition, energyToLeave);
            if (isHalfPositionFreeFromRobots && isHasEnergyToGoToHalfPosition)
                return halfPosition;

            Position quarterPosition = FindQuarterPositionOnTheWay(movingRobot.Position, positionToGo);
            bool isQuarterPositionFreeFromRobots =
                ValidationHelper.IsCellFreeFromRobots(quarterPosition, movingRobot, robots, map);
            bool isHasEnergyToGoToQuarterPosition =
                ValidationHelper.IsHasEnergyToGo(movingRobot, quarterPosition, energyToLeave);
            if (isQuarterPositionFreeFromRobots && isHasEnergyToGoToQuarterPosition)
                return quarterPosition;

            return null;
        }
        public static EnergyStation FindStationByPosition( Map map, Position position)
        {
            if (position == null)
                return null;
            return map.Stations.ToList().Find((s) => s.Position == position);
        }
        public class EnemyComparer : IComparer<Robot.Common.Robot>
        {
            public Robot.Common.Robot myRobot { get; set; }
            public int Compare(Robot.Common.Robot x, Robot.Common.Robot y)
            {
                int profit1 =
                (int)(x.Energy * 0.1) - (CalculationHelper.CalculateEnergyBetweenCells(x.Position, myRobot.Position) + 30);
                int profit2 =
                (int)(y.Energy * 0.1) - (CalculationHelper.CalculateEnergyBetweenCells(y.Position, myRobot.Position) + 30);
                if (profit1 > profit2)
                    return 1;
                else if (profit1 < profit2)
                    return -1;
                else
                    return 0;
            }
        }
    }
}

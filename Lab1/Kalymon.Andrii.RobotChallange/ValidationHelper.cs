﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Kalymon.Andrii.RobotChallange
{
    public class ValidationHelper
    {
        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
        IList<Robot.Common.Robot> robots, Map map)
        {
            bool flagOfFree = false;
            List<Position> positions = CalculationHelper.CalculateCellsAroundStation(station.Position, map);
            foreach (Position p in positions)
                if (IsCellFreeFromRobots(p, movingRobot, robots, map) == false)
                    flagOfFree = true;
            if (flagOfFree)
                return false;
            else
                return true;
        }
        public static bool IsCellFreeFromRobots(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, Map map)
        {
            if (map.IsValid(cell))
            {
                foreach (var robot in robots)
                {
                    if (robot != movingRobot)
                    {
                        if (robot.Position == cell)
                            return false;
                    }
                }
                return true;
            }
            return false;
        }
        public static bool IsNormalToAttack( Robot.Common.Robot enemyRobot, Robot.Common.Robot movingRobot, int profit)
        {
            int distance = CalculationHelper.CalculateEnergyBetweenCells(enemyRobot.Position, movingRobot.Position);
            if ((enemyRobot.Energy * 0.1 - distance - 30 <= profit) || (enemyRobot.OwnerName == movingRobot.OwnerName))
                return false;
            return enemyRobot == null ? false : true;
        }
        public static bool IsHasEnergyToGo(Robot.Common.Robot movingRobot, Position position2, int energyToLeave)
        {
            int energyToGo = CalculationHelper.CalculateEnergyBetweenCells(movingRobot.Position, position2);
            if (((movingRobot.Energy - energyToGo) >= energyToLeave))
                return true;
            return false;
        }
    }
}

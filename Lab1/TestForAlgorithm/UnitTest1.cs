﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;
using Kalymon.Andrii.RobotChallange;

namespace TestForAlgorithm
{
    [TestClass]
    public class TestFindingHelper
    {
        [TestMethod]
        public void Test_EnemyComparer()
        {
            //Arrange
            Robot.Common.Robot myRobot = new Robot.Common.Robot() { Energy = 200, Position = new Position(0, 0) };
            Robot.Common.Robot r1 = new Robot.Common.Robot() { Energy = 600, Position = new Position(3, 4) };
            Robot.Common.Robot r2 = new Robot.Common.Robot() { Energy = 500, Position = new Position(3, 1) };
            IComparer<Robot.Common.Robot> comparer = new FindingHelper.EnemyComparer() { myRobot = myRobot };
            int expected = -1;

            //Act
            int result = comparer.Compare(r1, r2);
            
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void Test_FindEnemies()
        {
            //Arrange
            string ownerName = "Andrii";
            Robot.Common.Robot myRobot1 = new Robot.Common.Robot()
            { Energy = 100, Position = new Position(1, 1), OwnerName = ownerName };
            Robot.Common.Robot myRobot2 = new Robot.Common.Robot()
            { Energy = 100, Position = new Position(2, 1), OwnerName = ownerName };
            Robot.Common.Robot enemy1 = new Robot.Common.Robot()
            { Energy = 100, Position = new Position(1, 1), OwnerName = "En" };
            Robot.Common.Robot enemy2 = new Robot.Common.Robot()
            { Energy = 100, Position = new Position(2, 1), OwnerName = "En" };
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            { myRobot1, enemy1, enemy2, myRobot2 };

            //Act
            List<Robot.Common.Robot> enemies = FindingHelper.FindEnemies(robots, ownerName);
            Robot.Common.Robot result = enemies.Find((r) => r.OwnerName == ownerName);

            //Assert
            Assert.IsNull(result);
        }
        [TestMethod]
        public void Test_FindMostProfitRobotToAttack()
        {
            //Arrange
            Robot.Common.Robot myRobot = new Robot.Common.Robot()
            { Energy = 100, Position = new Position(0, 0), OwnerName = "Andrii" };
            Robot.Common.Robot r1 = new Robot.Common.Robot()
            { Energy = 1400, Position = new Position(2, 3), OwnerName = "r1" };
            Robot.Common.Robot r2 = new Robot.Common.Robot()
            { Energy = 1000, Position = new Position(2, 1), OwnerName = "r2" };
            Robot.Common.Robot r3 = new Robot.Common.Robot()
            { Energy = 1300, Position = new Position(1, 1), OwnerName = "r3" };
            Robot.Common.Robot r4 = new Robot.Common.Robot()
            { Energy = 800, Position = new Position(3, 4), OwnerName = "r4" };
            Robot.Common.Robot r5 = new Robot.Common.Robot()
            { Energy = 5000, Position = new Position(8, 7), OwnerName = "r8" };
            List<Robot.Common.Robot> enemies = new List<Robot.Common.Robot>()
            { r2, r1, r4, r5, r3 };
            string expected = "r3";

            //Act
            Robot.Common.Robot res = FindingHelper.FindMostProfitRobotToAttack(enemies, myRobot);

            //Assert
            Assert.AreEqual(expected, res.OwnerName);
        }
        [TestMethod]
        public void Test_IsNormalToAttack()
        {
            //Arrange
            Robot.Common.Robot robotToAttack = new Robot.Common.Robot()
            { Energy = 500, Position = new Position(5, 6), OwnerName="Enemy" };
            Robot.Common.Robot movingRobot = new Robot.Common.Robot()
            { Energy = 400, Position = new Position(3, 4), OwnerName="I" };

            //Act 
            bool actual = ValidationHelper.IsNormalToAttack(robotToAttack, movingRobot, 11);

            //Assert
            Assert.IsTrue(actual);
        }
        [TestMethod]
        public void Test_FindHalfPositionOnTheWay()
        {
            //Arrange
            Robot.Common.Robot movingRobot = new Robot.Common.Robot()
            { Energy = 400, Position = new Position(3, 4), OwnerName = "I" };
            Position positionToGo = new Position(7, 18);
            Position expected = new Position(5, 11);

            //Act 
            Position half = FindingHelper.FindHalfPositionOnTheWay(movingRobot.Position, positionToGo);
            bool actual = false;
            if (half.X == expected.X && half.Y == expected.Y)
                actual = true;

            //Assert
            Assert.IsTrue(actual);
        }
        [TestMethod]
        public void Test_CalculateCellsAroundStation()
        {
            //Arrange
            Position stationPosition = new Position(3, 4);
            Map map = new Map()
            { MinPozition = new Position(0,0), MaxPozition = new Position(99,99) };
            List<Position> expectedPositions = new List<Position>()
            {
                new Position(1,4), new Position(2,4), new Position(3,4), new Position(4,4), new Position(5,4),
                new Position(2,5), new Position(3,5), new Position(4,5), new Position(3,6),
                new Position(2,3), new Position(3,3), new Position(4,3), new Position(3,2),                 
            };

            //Act 
            List<Position> actualPositions =
            CalculationHelper.CalculateCellsAroundStation(stationPosition, map);

            //Assert
            //Assert.IsNotNull(actualPositions);
            CollectionAssert.AreEquivalent(expectedPositions, actualPositions);
        }
        [TestMethod]
        public void Test_IsCellFreeFromRobots()
        {
            //Arrange
            Robot.Common.Robot movingRobot = new Robot.Common.Robot() { Position = new Position(1, 3) };
            Position cell = new Position(4, 3);
            Map map = new Map()
            { MinPozition = new Position(0, 0), MaxPozition = new Position(99, 99) };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Position = new Position(4,4) },
                new Robot.Common.Robot() {Position = new Position(4,5) },
                new Robot.Common.Robot() {Position = new Position(5,5) }
            };

            //Act
            bool result = ValidationHelper.IsCellFreeFromRobots(cell, movingRobot, robots, map);

            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void Test_IsStationFree()
        {
            //Arrange
            Map map = new Map()
            { MinPozition = new Position(0, 0), MaxPozition = new Position(99, 99) };
            EnergyStation energyStation = new EnergyStation() { Position = new Position(4,4) };
            Robot.Common.Robot movingRobot = new Robot.Common.Robot()
            { Position = new Position(3, 4) };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Position = new Position(10,4) },
                new Robot.Common.Robot() {Position = new Position(4,5) },
                new Robot.Common.Robot() {Position = new Position(78,5) }
            };

            //Act
            bool result = ValidationHelper.IsStationFree(energyStation, movingRobot, robots, map);

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void Test_FindQuarterPositionOnTheWay()
        {
            //Arrange
            Robot.Common.Robot movingRobot = new Robot.Common.Robot()
            { Energy = 400, Position = new Position(2, 4), OwnerName = "I" };
            Position positionToGo = new Position(6, 10);
            List<int> expected = new List<int>() { 3, 5 };

            //Act 
            Position quarter = FindingHelper.FindQuarterPositionOnTheWay(movingRobot.Position, positionToGo);
            List<int> actual = new List<int>() { quarter.X, quarter.Y };

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Test_FindPossiblePositionToGo()
        {
            //Arrange
            Robot.Common.Robot movingRobot = new Robot.Common.Robot()
            { Energy = 40, Position = new Position(2, 4), OwnerName = "I" };
            Position positionToGo = new Position(6, 10);
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Position = new Position(4,8) },
                new Robot.Common.Robot() {Position = new Position(4,5) },
                new Robot.Common.Robot() {Position = new Position(5,5) }
            };
            Map map = new Map()
            { MinPozition = new Position(0, 0), MaxPozition = new Position(99, 99) };
            List<int> expected = new List<int>() { 4, 7 };

            //Act 
            Position possible =
            FindingHelper.FindPossiblePositionToGo(movingRobot, robots, map, positionToGo, 1);
            List<int> actual = new List<int>() { possible.X, possible.Y };

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Test_FindStationByPosition()
        {
            //Arrange
            Map map = new Map()
            { MinPozition = new Position(0, 0), MaxPozition = new Position(99, 99) };
            EnergyStation energyStation = new EnergyStation() { Position = new Position(4, 4) };
            map.Stations.Add(energyStation);

            //Act
            EnergyStation result = FindingHelper.FindStationByPosition(map, new Position(4, 4));

            //Assert
            Assert.IsNotNull(result);
        }
    }
}

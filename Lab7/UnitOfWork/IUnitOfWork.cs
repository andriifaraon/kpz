﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.Models;
using Lab7.Repository;

namespace Lab7.UnitOfWork
{
    public interface IUnitOfWork: IDisposable
    {
        IRepository<EstateAgent, string> EstateAgentsRepository { get; }
        IRepository<AccessType, int> AccessTypesRepository { get; }
        void SaveChanges();
    }
}

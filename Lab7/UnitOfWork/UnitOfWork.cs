﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab7.Models;
using Lab7.Repository;

namespace Lab7.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool isDisposed;

        private EstateAgentContext estateAgentContext;

        public UnitOfWork(DbContext context)
        {
            estateAgentContext = (EstateAgentContext)context;
            EstateAgentsRepository = new EstateAgentRepository(context);
            AccessTypesRepository = new AccessTypeRepository(context);
        }

        public IRepository<EstateAgent, string> EstateAgentsRepository { get; set; }

        public IRepository<AccessType, int> AccessTypesRepository { get; set; }


        public void SaveChanges()
        {
            estateAgentContext.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    estateAgentContext.Dispose();
                }

                isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
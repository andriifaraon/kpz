﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Lab7.Models;
using Lab7.UnitOfWork;

namespace Lab7.Controllers
{
    public class AccessTypesController : ApiController
    {
        private IUnitOfWork unitOfWork;

        public AccessTypesController()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(new EstateAgentContext());
        }

        public AccessTypesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: api/AccessTypes
        public IEnumerable<AccessType> GetAccessTypes() => unitOfWork.AccessTypesRepository.GetAll();

        // GET: api/AccessTypes/5
        [ResponseType(typeof(AccessType))]
        public IHttpActionResult GetAccessType(int id)
        {
            AccessType accessType = unitOfWork.AccessTypesRepository.Get(id);
            if (accessType == null)
            {
                return NotFound();
            }

            return Ok(accessType);
        }

        // PUT: api/AccessTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccessType(int id, AccessType accessType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accessType.AccessID)
            {
                return BadRequest();
            }

            unitOfWork.AccessTypesRepository.Update(accessType);
            unitOfWork.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccessTypes
        [ResponseType(typeof(AccessType))]
        public IHttpActionResult PostAccessType(AccessType accessType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            unitOfWork.AccessTypesRepository.Create(accessType);
            unitOfWork.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = accessType.AccessID }, accessType);
        }

        // DELETE: api/AccessTypes/5
        [ResponseType(typeof(AccessType))]
        public IHttpActionResult DeleteAccessType(int id)
        {
            unitOfWork.AccessTypesRepository.Delete(id);
            unitOfWork.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }

        private bool AccessTypeExists(int id) => unitOfWork.AccessTypesRepository.GetAll().Count(e => e.AccessID == id) > 0;
    }
}
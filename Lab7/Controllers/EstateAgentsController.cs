﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Lab7.Models;
using Lab7.UnitOfWork;
using Lab7.Repository;

namespace Lab7.Controllers
{
    public class EstateAgentsController : ApiController
    {
        private IUnitOfWork unitOfWork;

        public EstateAgentsController()
        {
            unitOfWork = new UnitOfWork.UnitOfWork(new EstateAgentContext());
        }

        public EstateAgentsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: api/EstateAgents
        public IEnumerable<EstateAgent> GetEstateAgents() => unitOfWork.EstateAgentsRepository.GetAll();


        // GET: api/EstateAgents/5
        [ResponseType(typeof(EstateAgent))]
        public IHttpActionResult GetEstateAgent(string id)
        {
            EstateAgent estateAgent = unitOfWork.EstateAgentsRepository.Get(id);
            if (estateAgent == null)
            {
                return NotFound();
            }

            return Ok(estateAgent);
        }

        // PUT: api/EstateAgents/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEstateAgent(string id, EstateAgent estateAgent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != estateAgent.AgentEmail)
            {
                return BadRequest();
            }

            unitOfWork.EstateAgentsRepository.Update(estateAgent);
            unitOfWork.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EstateAgents
        [ResponseType(typeof(EstateAgent))]
        public IHttpActionResult PostEstateAgent(EstateAgent estateAgent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            unitOfWork.EstateAgentsRepository.Create(estateAgent);
            unitOfWork.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = estateAgent.AgentEmail }, estateAgent);
        }

        // DELETE: api/EstateAgents/5
        [ResponseType(typeof(EstateAgent))]
        public IHttpActionResult DeleteEstateAgent(string id)
        {
            unitOfWork.EstateAgentsRepository.Delete(id);
            unitOfWork.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }

        private bool EstateAgentExists(string id)  => unitOfWork.EstateAgentsRepository.GetAll().Count(e => e.AgentEmail == id) > 0;
    }
}
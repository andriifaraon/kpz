﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab7.Models;

namespace Lab7.Initializers
{
    public class EstateAgentInitializer: DropCreateDatabaseAlways<EstateAgentContext>
    {
        protected override void Seed(EstateAgentContext db)
        {
            db.EstateAgents.Add(new EstateAgent
            { AgentEmail = "agent007@gmail.com", Name = "Andre", Surname="Faro" , PhoneNumber="380979873453", Sum = 0, CountOfContracts=0 });
            db.EstateAgents.Add(new EstateAgent
            { AgentEmail = "girl007@gmail.com", Name = "Lina", Surname = "Petr", PhoneNumber = "380979873053", Sum = 0, CountOfContracts = 0 });
            db.EstateAgents.Add(new EstateAgent
            { AgentEmail = "worker5@gmail.com", Name = "Lana", Surname = "Monte", PhoneNumber = "380970873453", Sum = 0, CountOfContracts = 0 });
            base.Seed(db);
        }
    }
}
﻿using Lab7.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Lab7.Initializers
{
    public class AccessTypeInitializer : DropCreateDatabaseAlways<EstateAgentContext>
    {
        protected override void Seed(EstateAgentContext db)
        {
            db.AccessTypes.Add(
                new AccessType() { AccessID = 78, TypeName = "Vip", TypeDescription = "No" });
            db.AccessTypes.Add(
                new AccessType() { AccessID = 80, TypeName = "Partly", TypeDescription = "Partly access" });
            base.Seed(db);
        }
    }
}
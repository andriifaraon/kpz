﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Lab7.Models
{
    public class EstateAgentContext: DbContext
    {
        public EstateAgentContext() : base("DefaultConnection")
        {
        }
        public DbSet<EstateAgent> EstateAgents { get; set; }
        public DbSet<AccessType> AccessTypes { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Lab7.Models
{
    public class AccessType
    {
        [Key]
        public int AccessID { get; set; }
        public string TypeName { get; set; }
        public string TypeDescription { get; set; }
    }
}
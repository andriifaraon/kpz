﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Lab7.Models
{
    public class EstateAgent
    {
        [Key]
        [EmailAddress(ErrorMessage = "It isn`t email")]
        public string AgentEmail { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public decimal? Sum { get; set; }
        [Range(1, 30000, ErrorMessage = "Invalid Range")]
        public int? CountOfContracts { get; set; }
    }
}
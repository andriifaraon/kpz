﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab7.Models;

namespace Lab7.Repository
{
    public class AccessTypeRepository : IRepository<AccessType, int>
    {
        private readonly EstateAgentContext estateAgentContext;
        public AccessTypeRepository(DbContext context)
        {
            estateAgentContext = (EstateAgentContext)context;
        }
        public void Create(AccessType item) => estateAgentContext.AccessTypes.Add(item);

        public void Delete(int id)
        {
            var accessType = estateAgentContext.AccessTypes.Find(id);
            if (accessType != null)
                estateAgentContext.AccessTypes.Remove(accessType);
        }

        public AccessType Get(int id) => estateAgentContext.AccessTypes.Find(id);

        public IEnumerable<AccessType> GetAll() => estateAgentContext.AccessTypes;

        public void Update(AccessType item) => estateAgentContext.Entry(item).State = EntityState.Modified;
    }
}
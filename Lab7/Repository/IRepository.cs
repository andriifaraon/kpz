﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7.Repository
{
    public interface IRepository<T, U> where T: class
    {
        IEnumerable<T> GetAll();
        T Get(U id);
        void Create(T item);
        void Update(T item);
        void Delete(U id);
    }
}

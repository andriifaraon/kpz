﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Lab7.Models;

namespace Lab7.Repository
{
    public class EstateAgentRepository : IRepository<EstateAgent, string>
    {
        private readonly EstateAgentContext estateAgentContext;
        public EstateAgentRepository(DbContext context)
        {
            estateAgentContext = (EstateAgentContext)context;
        }
        public void Create(EstateAgent item) =>  estateAgentContext.EstateAgents.Add(item);

        public void Delete(string id)
        {
            var estateAgent = estateAgentContext.EstateAgents.Find(id);
            if (estateAgent != null)
                estateAgentContext.EstateAgents.Remove(estateAgent);
        }

        public EstateAgent Get(string id) => estateAgentContext.EstateAgents.Find(id);

        public IEnumerable<EstateAgent> GetAll() => estateAgentContext.EstateAgents;

        public void Update(EstateAgent item) => estateAgentContext.Entry(item).State = EntityState.Modified;
    }
}
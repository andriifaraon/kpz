﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class EstateObject
    {
        public static int maxSumPerDay;
        int sumPerDay;
        int id;
        public int Id
        {
            get => id;
            set
            {
                if (value < 0)
                    value = 0;
                id = value;
            }
        }
        public int OwnerId { get; set; }
        public EstateType TypeOfEstate { get; set; }
        public string City { get; set; }
        public int SumPerDay
        {
            get => sumPerDay;
            set
            {
                if (value > maxSumPerDay)
                    value = maxSumPerDay;
                sumPerDay = value;
            }
        }
        private Company company;
        static EstateObject()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 10_000_000; i++)
            {
                maxSumPerDay = 1700;
            }
            stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }
        class Company
        {
            public string CompanyName { get; set; }
            public string CompanyCity { get; set; }
            public Company(string companyName, string companyCity)
            {
                CompanyCity = companyCity;
                CompanyName = companyName;
            }
        }
        public EstateObject()
        {
            Id = default;
            OwnerId = default;
            TypeOfEstate = default;
            City = "no record";
            SumPerDay = default;
            company = new Company("no record", "no record");
        }
        public EstateObject(int id, int ownerId): this()
        {
            Id = id;
            OwnerId = ownerId;
        }
        public EstateObject(int id, int ownerId, EstateType estateType, string city,
            string companyName, string companyCity, int sumPerDay): this(id, ownerId)
        {
            TypeOfEstate = estateType;
            City = city;
            SumPerDay = sumPerDay;
            company = new Company(companyName, companyCity);
        }
        public EstateObject(int id, int ownerId, EstateType estateType, string city, int sumPerDay)
        {
            OwnerId = ownerId;
            Id = id;
            TypeOfEstate = estateType;
            City = city;
            SumPerDay = sumPerDay;
        }
        public override string ToString()
        {
            return Id + " " + OwnerId + " " + TypeOfEstate + " " + City + " " + company.CompanyName + " " + SumPerDay;
        }
        public bool IsForLiving()
        {
            EstateType estateTypeFroLiving = EstateType.Flat | EstateType.House;
            return (estateTypeFroLiving & TypeOfEstate) == TypeOfEstate;
        }
        public void BaseCity( out string companyCity, out string estateCity )
        {
            companyCity = company.CompanyCity;
            estateCity = City;
        }
        public static void SetAnonymous(ref EstateObject estateObject)
        {
            estateObject = new EstateObject();
        }
    }
    [Flags]
    enum EstateType
    {
        CommonEstate = 0,
        House = 1,
        Flat = 2,
        Store = 4, 
        Factory = 8,
        Stadium = 16
    }
}

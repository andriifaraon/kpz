﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    interface IEstateRentable
    {
        void Rent(DateTime start, DateTime end, EstateObject estateObject);
    }
    class Customer: Client, IEstateRentable
    {
        private double sum;
        public double Sum
        {
            get => sum;
            set
            {
                if (value < 0)
                    value = 0;
                sum = value;
            }
        }
        public Customer() : base()
        {
            Sum = 0;
        }
        public Customer(double sum) : base()
        {
            Sum = sum;
        }
        public Customer(int id, string name, string surname) : base(id, name, surname)
        {
            Sum = 0;
        }
        public Customer(int id, string name, string surname, double sum) : this(id, name, surname)
        {
            Sum = sum;
        }

        public override double GetTax()
        {
            return 0.12 * Sum;
        }
        public override string ToString()
        {
            return base.id + " " + base.name + " " + base.surname + " " + this.Sum;
        }

        public void Rent(DateTime start, DateTime end, EstateObject estateObject)
        {
            TimeSpan delta = end - start;
            double price = delta.Days * estateObject.SumPerDay;
            Console.WriteLine("You must pay " + price);
            if( price > Sum )
                Console.WriteLine("You don`t have money, your sum is " + Sum);
            else
                Console.WriteLine("You have money, your sum is " + Sum);
        }

        public static explicit operator double(Customer customer ) => customer.Sum;
        public static implicit operator Customer(double sum) => new Customer(sum);
    }
}

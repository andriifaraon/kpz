﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class EstateOwner: Client
    { 
        public int EstateCount { get; private set; }
        public EstateOwner(): base()
        {
            EstateCount = 0;
        }
        public EstateOwner(int id, string name, string surname): base(id, name, surname)
        {
            EstateCount = 0;
        }
        public override double GetTax()
        {
            double tax = EstateCount * 40;
            if (tax > 200)
                return 200;
            return tax;
        }
        public override string ToString()
        {
            return base.id + " " + base.name + " " + base.surname + " " + this.EstateCount;
        }
    }
}

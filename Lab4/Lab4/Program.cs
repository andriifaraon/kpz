﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer = new Customer(1, "Andre", "Kalymon") { Sum = 14000 };
            EstateType estateType = EstateType.House | EstateType.Flat;
            EstateObject estateObject1 = new EstateObject(1, 1, estateType, "Lviv", "ReHouse", "Kyiv", 14500);
            EstateObject estateObject2 = new EstateObject(2, 1, EstateType.Factory, "Lviv", "ReHouse", "Rivne", 600);
            customer.Rent(DateTime.Parse("03.12.2020"), DateTime.Parse("6.12.2020"), estateObject1);
            Console.WriteLine(estateObject1);
            Console.WriteLine("Is estateObject2 for living: " + estateObject2.IsForLiving());
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    abstract class Client: ITaxesPayeble
    {
        protected int id;
        protected string name;
        protected string surname;
        public Client()
        {
            id = default;
            name = "no record";
            surname = "no record";
        }
        public Client(int id, string name, string surname)
        {
            this.id = id;
            this.name = name;
            this.surname = surname;
        }

        public abstract double GetTax();
    }
    interface ITaxesPayeble
    {
        double GetTax();
    }
}

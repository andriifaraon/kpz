﻿using AutoMapper;
using RentUp.Model;
using RentUp.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentUp.UI.Base
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel>();

            Mapper.CreateMap<Client, ClientViewModel>();
            Mapper.CreateMap<ClientViewModel, Client>();

            Mapper.CreateMap<Agent, AgentViewModel>();
            Mapper.CreateMap<AgentViewModel, Agent>();

            Mapper.CreateMap<Space, SpaceViewModel>();
            Mapper.CreateMap<SpaceViewModel, Space>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using RentUp.Model;

namespace RentUp.UI.Convertors
{
    public class ImagePathConverter : IValueConverter
    {
        Dictionary<SpaceType, BitmapImage> cache = new Dictionary<SpaceType, BitmapImage>();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = (SpaceType)value;
            if(!cache.ContainsKey(type))
            {
                var uri = new Uri(string.Format(@"../Images/{0}/{1}.png", parameter, type), UriKind.Relative);
                cache.Add(type, new BitmapImage(uri));
            }
            
            return cache[type];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

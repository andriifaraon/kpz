﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentUp.Model;

namespace RentUp.UI.ViewModels
{
    public class ClientViewModel : ViewModelBase
    {
        private int _id;
        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        private string _surname;
        public string Surname
        {
            get => _surname;
            set
            {
                _surname = value;
                OnPropertyChanged(nameof(Surname));
            }
        }
        private ObservableCollection<SpaceViewModel> _spaces;
        public ObservableCollection<SpaceViewModel> Spaces
        {
            get => _spaces;
            set
            {
                _spaces = value;
                OnPropertyChanged(nameof(Spaces));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentUp.Model;

namespace RentUp.UI.ViewModels
{
    public class SpaceViewModel: ViewModelBase
    {
        private int _id;
        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged(nameof(Id));
            }
        }
        private string _city;
        public string City
        {
            get => _city;
            set
            {
                _city = value;
                OnPropertyChanged(nameof(City));
            }
        }
        private double _pricePerDay;
        public double PricePerDay
        {
            get => _pricePerDay;
            set
            {
                _pricePerDay = value;
                OnPropertyChanged(nameof(PricePerDay));
            }
        }
        private double _square;
        public double Square
        {
            get => _square;
            set
            {
                _square = value;
                OnPropertyChanged(nameof(Square));
            }
        }
        private SpaceType _spaceType;
        public SpaceType SpaceType
        {
            get => _spaceType;
            set
            {
                _spaceType = value;
                OnPropertyChanged(nameof(SpaceType));
            }
        }
        public override string ToString()
        {
            return Id + " " + City;
        }
    }
}

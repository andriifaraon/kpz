﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RentUp.Model;

namespace RentUp.UI.ViewModels
{
    public class DataViewModel: ViewModelBase
    {
        public DataViewModel()
        {
            SetControlVisibilityCommand = new Command(SetControlVisibility);
            SetTypeCommand = new Command(SetType);
            DeleteSpaceCommand = new Command(DeleteSpace);
            DeleteAgentCommand = new Command(DeleteAgent);
        }

        private string _visibleControl = "Spaces";
        public string VisibleControl
        {
            get => _visibleControl;
            set
            {
                _visibleControl = value;
                OnPropertyChanged(nameof(VisibleControl));
            }
        }

        private SpaceViewModel _selectedSpace;
        public SpaceViewModel SelectedSpace
        {
            get => _selectedSpace;
            set
            {
                _selectedSpace = value;
                OnPropertyChanged(nameof(SelectedSpace));
            }
        }

        private AgentViewModel _selectedAgent;
        public AgentViewModel SelectedAgent
        {
            get => _selectedAgent;
            set
            {
                _selectedAgent = value;
                OnPropertyChanged(nameof(_selectedAgent));
            }
        }

        public ICommand SetControlVisibilityCommand { get; set; }
        public void SetControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }

        public ICommand DeleteSpaceCommand { get; set; }
        public void DeleteSpace(object args)
        {
            Spaces.Remove(SelectedSpace);
            SoundPlayer player = new SoundPlayer();
            player.Stream = Properties.Resources.voice1;
            player.Play();
        }

        public ICommand DeleteAgentCommand { get; set; }
        public void DeleteAgent(object args)
        {
            Agents.Remove(SelectedAgent);
            SoundPlayer player = new SoundPlayer();
            player.Stream = Properties.Resources.voice1;
            player.Play();
        }

        public ICommand SetTypeCommand { get; set; }
        public void SetType(object args)
        {
            string type = (string)args;
            if(SelectedSpace!= null)
            {
                if (type == "Factorying")
                    SelectedSpace.SpaceType = SpaceType.Factorying;
                else if (type == "Housing")
                    SelectedSpace.SpaceType = SpaceType.Housing;
                else
                    SelectedSpace.SpaceType = SpaceType.Sporting;
            }
        }

        private ObservableCollection<ClientViewModel> _clients;
        public ObservableCollection<ClientViewModel> Clients
        {
            get => _clients;
            set
            {
                _clients = value;
                OnPropertyChanged(nameof(Clients));
            }
        }

        private ObservableCollection<AgentViewModel> _agents;
        public ObservableCollection<AgentViewModel> Agents
        {
            get => _agents;
            set
            {
                _agents = value;
                OnPropertyChanged(nameof(Agents));
            }
        }

        private ObservableCollection<SpaceViewModel> _spaces;
        public ObservableCollection<SpaceViewModel> Spaces
        {
            get => _spaces;
            set
            {
                _spaces = value;
                OnPropertyChanged(nameof(Spaces));
            }
        }
    }
}

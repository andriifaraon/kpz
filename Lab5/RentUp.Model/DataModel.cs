﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentUp.Model.Serialization;

namespace RentUp.Model
{
    [Serializable]
    public class DataModel
    {
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<Space> Spaces { get; set; }
        public IEnumerable<Agent> Agents { get; set; }
        public DataModel()
        {
            
            Agents = new List<Agent>() { new Agent() { Name="Olia", Id=1, Surname="Kalymon"} };
            List<Space> spaces  = new List<Space>() { new Space() { Id = 1, City = "Lviv", PricePerDay = 250, Square = 45 },
                new Space() { Id = 2, City = "Komarno", PricePerDay = 250, Square = 45 } };
            Spaces = spaces;
            Clients = new List<Client>() { new Client() { Id = 2, Name = "Petro", Surname = "Most", Spaces = spaces } };
        }
        public static string DataPath = "rentup.dat";
        public static DataModel Load()
        {
            if(File.Exists(DataPath))
            {
                return DataSerializer.DeserializeData(DataPath);
            }
            return new DataModel();
        }
        public void Save()
        {
            DataSerializer.SerizlizeData(DataPath, this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace RentUp.Model.Serialization
{
    public class DataSerializer
    {
        public static void SerizlizeData(string fileName, DataModel data)
        {
            var binaryFormatter = new BinaryFormatter();
            var s = new FileStream(fileName, FileMode.Create);
            binaryFormatter.Serialize(s, data);
            s.Close();
        }
        public static DataModel DeserializeData(string fileName)
        {
            var binaryFormatter = new BinaryFormatter();
            var s = new FileStream(fileName, FileMode.Open);
            return (DataModel)binaryFormatter.Deserialize(s);
        }
    }
}

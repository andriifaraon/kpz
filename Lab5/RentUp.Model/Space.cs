﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace RentUp.Model
{
    [Serializable]
    public class Space
    {
        public int Id { get; set; }
        public string City { get; set; }
        public double PricePerDay { get; set; }
        public double Square { get; set; }
        public SpaceType SpaceType { get; set;}
    }
    [Serializable]
    public enum SpaceType
    {
        Housing,
        Factorying,
        Sporting
    }
}

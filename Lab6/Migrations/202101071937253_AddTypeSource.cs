namespace Lab6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTypeSource : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EstateTypes", "TypeSource", s => s.String(nullable: true));   
        }
        
        public override void Down()
        {
            DropColumn("dbo.EstateTypes", "TypeSource");
        }
    }
}

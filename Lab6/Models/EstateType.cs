﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    [Table("EstateTypes")]
    public class EstateType
    {
        [Key]
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public string TypeDescription { get; set; }
        public string TypeSource { get; set; }
    }
}

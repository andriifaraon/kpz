﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    [Table("EstateObjects")]
    public class EstateObject
    {
        [Key]
        public int ObjectID { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public int ObjectType { get; set; }
        public decimal? Square { get; set; }
        public string OwnerEmail { get; set; }
        public string Description { get; set; }
        public decimal Sum { get; set; }
        public int DaysDuration { get; set; }
    }
}

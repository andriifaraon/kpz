﻿namespace Lab6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.clientsTypesPage = new System.Windows.Forms.TabPage();
            this.saveClientTypeButton = new System.Windows.Forms.Button();
            this.deleteClientTypeButton = new System.Windows.Forms.Button();
            this.addClientTypeButton = new System.Windows.Forms.Button();
            this.clientTypesGridView = new System.Windows.Forms.DataGridView();
            this.cleanersPage = new System.Windows.Forms.TabPage();
            this.saveCleanerButton = new System.Windows.Forms.Button();
            this.deleteCleanerButton = new System.Windows.Forms.Button();
            this.addCleanerButton = new System.Windows.Forms.Button();
            this.cleanersGridView = new System.Windows.Forms.DataGridView();
            this.estateTypesTab = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.typeDescTextBox = new System.Windows.Forms.TextBox();
            this.typeNameTextBox = new System.Windows.Forms.TextBox();
            this.updateEstateType = new System.Windows.Forms.Button();
            this.deleteEstateType = new System.Windows.Forms.Button();
            this.addEstateType = new System.Windows.Forms.Button();
            this.estateTypesGridView = new System.Windows.Forms.DataGridView();
            this.estateObjectsPage = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.objectTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.houseNumberTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.streetTextBox = new System.Windows.Forms.TextBox();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.estateObjectsGridView = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.squareTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ownerEmailComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.daysDurationTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.sumTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.clientsTypesPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientTypesGridView)).BeginInit();
            this.cleanersPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cleanersGridView)).BeginInit();
            this.estateTypesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.estateTypesGridView)).BeginInit();
            this.estateObjectsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.estateObjectsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.clientsTypesPage);
            this.tabControl1.Controls.Add(this.cleanersPage);
            this.tabControl1.Controls.Add(this.estateTypesTab);
            this.tabControl1.Controls.Add(this.estateObjectsPage);
            this.tabControl1.Location = new System.Drawing.Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1052, 684);
            this.tabControl1.TabIndex = 0;
            // 
            // clientsTypesPage
            // 
            this.clientsTypesPage.Controls.Add(this.saveClientTypeButton);
            this.clientsTypesPage.Controls.Add(this.deleteClientTypeButton);
            this.clientsTypesPage.Controls.Add(this.addClientTypeButton);
            this.clientsTypesPage.Controls.Add(this.clientTypesGridView);
            this.clientsTypesPage.Location = new System.Drawing.Point(4, 25);
            this.clientsTypesPage.Name = "clientsTypesPage";
            this.clientsTypesPage.Padding = new System.Windows.Forms.Padding(3);
            this.clientsTypesPage.Size = new System.Drawing.Size(1044, 655);
            this.clientsTypesPage.TabIndex = 0;
            this.clientsTypesPage.Text = "ClientsTypes";
            this.clientsTypesPage.UseVisualStyleBackColor = true;
            // 
            // saveClientTypeButton
            // 
            this.saveClientTypeButton.Location = new System.Drawing.Point(915, 553);
            this.saveClientTypeButton.Name = "saveClientTypeButton";
            this.saveClientTypeButton.Size = new System.Drawing.Size(120, 64);
            this.saveClientTypeButton.TabIndex = 3;
            this.saveClientTypeButton.Text = "Save";
            this.saveClientTypeButton.UseVisualStyleBackColor = true;
            this.saveClientTypeButton.Click += new System.EventHandler(this.SaveClientTypeButton_Click);
            // 
            // deleteClientTypeButton
            // 
            this.deleteClientTypeButton.Location = new System.Drawing.Point(762, 553);
            this.deleteClientTypeButton.Name = "deleteClientTypeButton";
            this.deleteClientTypeButton.Size = new System.Drawing.Size(120, 64);
            this.deleteClientTypeButton.TabIndex = 2;
            this.deleteClientTypeButton.Text = "Delete";
            this.deleteClientTypeButton.UseVisualStyleBackColor = true;
            this.deleteClientTypeButton.Click += new System.EventHandler(this.DeleteClientTypeButton_Click);
            // 
            // addClientTypeButton
            // 
            this.addClientTypeButton.Location = new System.Drawing.Point(603, 553);
            this.addClientTypeButton.Name = "addClientTypeButton";
            this.addClientTypeButton.Size = new System.Drawing.Size(120, 64);
            this.addClientTypeButton.TabIndex = 1;
            this.addClientTypeButton.Text = "Add";
            this.addClientTypeButton.UseVisualStyleBackColor = true;
            this.addClientTypeButton.Click += new System.EventHandler(this.AddClientTypeButton_Click);
            // 
            // clientTypesGridView
            // 
            this.clientTypesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientTypesGridView.Location = new System.Drawing.Point(6, 6);
            this.clientTypesGridView.Name = "clientTypesGridView";
            this.clientTypesGridView.RowHeadersWidth = 51;
            this.clientTypesGridView.RowTemplate.Height = 24;
            this.clientTypesGridView.Size = new System.Drawing.Size(591, 611);
            this.clientTypesGridView.TabIndex = 0;
            // 
            // cleanersPage
            // 
            this.cleanersPage.Controls.Add(this.saveCleanerButton);
            this.cleanersPage.Controls.Add(this.deleteCleanerButton);
            this.cleanersPage.Controls.Add(this.addCleanerButton);
            this.cleanersPage.Controls.Add(this.cleanersGridView);
            this.cleanersPage.Location = new System.Drawing.Point(4, 25);
            this.cleanersPage.Name = "cleanersPage";
            this.cleanersPage.Padding = new System.Windows.Forms.Padding(3);
            this.cleanersPage.Size = new System.Drawing.Size(1044, 655);
            this.cleanersPage.TabIndex = 1;
            this.cleanersPage.Text = "Cleaners";
            this.cleanersPage.UseVisualStyleBackColor = true;
            // 
            // saveCleanerButton
            // 
            this.saveCleanerButton.Location = new System.Drawing.Point(915, 553);
            this.saveCleanerButton.Name = "saveCleanerButton";
            this.saveCleanerButton.Size = new System.Drawing.Size(120, 64);
            this.saveCleanerButton.TabIndex = 6;
            this.saveCleanerButton.Text = "Save";
            this.saveCleanerButton.UseVisualStyleBackColor = true;
            this.saveCleanerButton.Click += new System.EventHandler(this.SaveCleanerButton_Click);
            // 
            // deleteCleanerButton
            // 
            this.deleteCleanerButton.Location = new System.Drawing.Point(762, 553);
            this.deleteCleanerButton.Name = "deleteCleanerButton";
            this.deleteCleanerButton.Size = new System.Drawing.Size(120, 64);
            this.deleteCleanerButton.TabIndex = 5;
            this.deleteCleanerButton.Text = "Delete";
            this.deleteCleanerButton.UseVisualStyleBackColor = true;
            this.deleteCleanerButton.Click += new System.EventHandler(this.DeleteCleanerButton_Click);
            // 
            // addCleanerButton
            // 
            this.addCleanerButton.Location = new System.Drawing.Point(603, 553);
            this.addCleanerButton.Name = "addCleanerButton";
            this.addCleanerButton.Size = new System.Drawing.Size(120, 64);
            this.addCleanerButton.TabIndex = 4;
            this.addCleanerButton.Text = "Add";
            this.addCleanerButton.UseVisualStyleBackColor = true;
            this.addCleanerButton.Click += new System.EventHandler(this.AddCleanerButton_Click);
            // 
            // cleanersGridView
            // 
            this.cleanersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cleanersGridView.Location = new System.Drawing.Point(6, 6);
            this.cleanersGridView.Name = "cleanersGridView";
            this.cleanersGridView.RowHeadersWidth = 51;
            this.cleanersGridView.RowTemplate.Height = 24;
            this.cleanersGridView.Size = new System.Drawing.Size(591, 611);
            this.cleanersGridView.TabIndex = 1;
            // 
            // estateTypesTab
            // 
            this.estateTypesTab.Controls.Add(this.label2);
            this.estateTypesTab.Controls.Add(this.label1);
            this.estateTypesTab.Controls.Add(this.typeDescTextBox);
            this.estateTypesTab.Controls.Add(this.typeNameTextBox);
            this.estateTypesTab.Controls.Add(this.updateEstateType);
            this.estateTypesTab.Controls.Add(this.deleteEstateType);
            this.estateTypesTab.Controls.Add(this.addEstateType);
            this.estateTypesTab.Controls.Add(this.estateTypesGridView);
            this.estateTypesTab.Location = new System.Drawing.Point(4, 25);
            this.estateTypesTab.Name = "estateTypesTab";
            this.estateTypesTab.Size = new System.Drawing.Size(1044, 655);
            this.estateTypesTab.TabIndex = 2;
            this.estateTypesTab.Text = "EstateTypes";
            this.estateTypesTab.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(865, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "Type Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(910, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Type Name";
            // 
            // typeDescTextBox
            // 
            this.typeDescTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.typeDescTextBox.Location = new System.Drawing.Point(834, 138);
            this.typeDescTextBox.Name = "typeDescTextBox";
            this.typeDescTextBox.Size = new System.Drawing.Size(201, 30);
            this.typeDescTextBox.TabIndex = 8;
            // 
            // typeNameTextBox
            // 
            this.typeNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.typeNameTextBox.Location = new System.Drawing.Point(834, 46);
            this.typeNameTextBox.Name = "typeNameTextBox";
            this.typeNameTextBox.Size = new System.Drawing.Size(201, 30);
            this.typeNameTextBox.TabIndex = 7;
            // 
            // updateEstateType
            // 
            this.updateEstateType.Location = new System.Drawing.Point(915, 553);
            this.updateEstateType.Name = "updateEstateType";
            this.updateEstateType.Size = new System.Drawing.Size(120, 64);
            this.updateEstateType.TabIndex = 6;
            this.updateEstateType.Text = "Update";
            this.updateEstateType.UseVisualStyleBackColor = true;
            this.updateEstateType.Click += new System.EventHandler(this.UpdateEstateType_Click);
            // 
            // deleteEstateType
            // 
            this.deleteEstateType.Location = new System.Drawing.Point(762, 553);
            this.deleteEstateType.Name = "deleteEstateType";
            this.deleteEstateType.Size = new System.Drawing.Size(120, 64);
            this.deleteEstateType.TabIndex = 5;
            this.deleteEstateType.Text = "Delete";
            this.deleteEstateType.UseVisualStyleBackColor = true;
            this.deleteEstateType.Click += new System.EventHandler(this.DeleteEstateType_Click);
            // 
            // addEstateType
            // 
            this.addEstateType.Location = new System.Drawing.Point(603, 553);
            this.addEstateType.Name = "addEstateType";
            this.addEstateType.Size = new System.Drawing.Size(120, 64);
            this.addEstateType.TabIndex = 4;
            this.addEstateType.Text = "Add";
            this.addEstateType.UseVisualStyleBackColor = true;
            this.addEstateType.Click += new System.EventHandler(this.AddEstateType_Click);
            // 
            // estateTypesGridView
            // 
            this.estateTypesGridView.AllowUserToAddRows = false;
            this.estateTypesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.estateTypesGridView.Location = new System.Drawing.Point(6, 6);
            this.estateTypesGridView.Name = "estateTypesGridView";
            this.estateTypesGridView.RowHeadersWidth = 51;
            this.estateTypesGridView.RowTemplate.Height = 24;
            this.estateTypesGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.estateTypesGridView.Size = new System.Drawing.Size(591, 611);
            this.estateTypesGridView.TabIndex = 2;
            // 
            // estateObjectsPage
            // 
            this.estateObjectsPage.Controls.Add(this.label9);
            this.estateObjectsPage.Controls.Add(this.daysDurationTextBox);
            this.estateObjectsPage.Controls.Add(this.label10);
            this.estateObjectsPage.Controls.Add(this.label11);
            this.estateObjectsPage.Controls.Add(this.sumTextBox);
            this.estateObjectsPage.Controls.Add(this.descriptionTextBox);
            this.estateObjectsPage.Controls.Add(this.label8);
            this.estateObjectsPage.Controls.Add(this.ownerEmailComboBox);
            this.estateObjectsPage.Controls.Add(this.label7);
            this.estateObjectsPage.Controls.Add(this.squareTextBox);
            this.estateObjectsPage.Controls.Add(this.label6);
            this.estateObjectsPage.Controls.Add(this.objectTypeComboBox);
            this.estateObjectsPage.Controls.Add(this.label5);
            this.estateObjectsPage.Controls.Add(this.houseNumberTextBox);
            this.estateObjectsPage.Controls.Add(this.label3);
            this.estateObjectsPage.Controls.Add(this.label4);
            this.estateObjectsPage.Controls.Add(this.streetTextBox);
            this.estateObjectsPage.Controls.Add(this.cityTextBox);
            this.estateObjectsPage.Controls.Add(this.button1);
            this.estateObjectsPage.Controls.Add(this.button2);
            this.estateObjectsPage.Controls.Add(this.button3);
            this.estateObjectsPage.Controls.Add(this.estateObjectsGridView);
            this.estateObjectsPage.Location = new System.Drawing.Point(4, 25);
            this.estateObjectsPage.Name = "estateObjectsPage";
            this.estateObjectsPage.Size = new System.Drawing.Size(1044, 655);
            this.estateObjectsPage.TabIndex = 3;
            this.estateObjectsPage.Text = "EstateObjects";
            this.estateObjectsPage.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(916, 189);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 25);
            this.label6.TabIndex = 18;
            this.label6.Text = "Object Type";
            // 
            // objectTypeComboBox
            // 
            this.objectTypeComboBox.FormattingEnabled = true;
            this.objectTypeComboBox.Location = new System.Drawing.Point(834, 217);
            this.objectTypeComboBox.Name = "objectTypeComboBox";
            this.objectTypeComboBox.Size = new System.Drawing.Size(201, 24);
            this.objectTypeComboBox.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(892, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 25);
            this.label5.TabIndex = 16;
            this.label5.Text = "House Number";
            // 
            // houseNumberTextBox
            // 
            this.houseNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.houseNumberTextBox.Location = new System.Drawing.Point(834, 156);
            this.houseNumberTextBox.Name = "houseNumberTextBox";
            this.houseNumberTextBox.Size = new System.Drawing.Size(201, 30);
            this.houseNumberTextBox.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(971, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 25);
            this.label3.TabIndex = 14;
            this.label3.Text = "Street";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(989, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "City";
            // 
            // streetTextBox
            // 
            this.streetTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.streetTextBox.Location = new System.Drawing.Point(834, 95);
            this.streetTextBox.Name = "streetTextBox";
            this.streetTextBox.Size = new System.Drawing.Size(201, 30);
            this.streetTextBox.TabIndex = 12;
            // 
            // cityTextBox
            // 
            this.cityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cityTextBox.Location = new System.Drawing.Point(834, 34);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(201, 30);
            this.cityTextBox.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(935, 570);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 47);
            this.button1.TabIndex = 9;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(811, 570);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 47);
            this.button2.TabIndex = 8;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(681, 570);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 47);
            this.button3.TabIndex = 7;
            this.button3.Text = "Add";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // estateObjectsGridView
            // 
            this.estateObjectsGridView.AllowUserToAddRows = false;
            this.estateObjectsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.estateObjectsGridView.Location = new System.Drawing.Point(6, 6);
            this.estateObjectsGridView.Name = "estateObjectsGridView";
            this.estateObjectsGridView.RowHeadersWidth = 51;
            this.estateObjectsGridView.RowTemplate.Height = 24;
            this.estateObjectsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.estateObjectsGridView.Size = new System.Drawing.Size(665, 611);
            this.estateObjectsGridView.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(959, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 25);
            this.label7.TabIndex = 20;
            this.label7.Text = "Square";
            // 
            // squareTextBox
            // 
            this.squareTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.squareTextBox.Location = new System.Drawing.Point(834, 273);
            this.squareTextBox.Name = "squareTextBox";
            this.squareTextBox.Size = new System.Drawing.Size(201, 30);
            this.squareTextBox.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(912, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 25);
            this.label8.TabIndex = 22;
            this.label8.Text = "Owner Email";
            // 
            // ownerEmailComboBox
            // 
            this.ownerEmailComboBox.FormattingEnabled = true;
            this.ownerEmailComboBox.Location = new System.Drawing.Point(834, 337);
            this.ownerEmailComboBox.Name = "ownerEmailComboBox";
            this.ownerEmailComboBox.Size = new System.Drawing.Size(201, 24);
            this.ownerEmailComboBox.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(892, 495);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 25);
            this.label9.TabIndex = 28;
            this.label9.Text = "Days Duration";
            // 
            // daysDurationTextBox
            // 
            this.daysDurationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.daysDurationTextBox.Location = new System.Drawing.Point(834, 523);
            this.daysDurationTextBox.Name = "daysDurationTextBox";
            this.daysDurationTextBox.Size = new System.Drawing.Size(201, 30);
            this.daysDurationTextBox.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(972, 431);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 25);
            this.label10.TabIndex = 26;
            this.label10.Text = "Sum";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(916, 370);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 25);
            this.label11.TabIndex = 25;
            this.label11.Text = "Description";
            // 
            // sumTextBox
            // 
            this.sumTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sumTextBox.Location = new System.Drawing.Point(834, 459);
            this.sumTextBox.Name = "sumTextBox";
            this.sumTextBox.Size = new System.Drawing.Size(201, 30);
            this.sumTextBox.TabIndex = 24;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionTextBox.Location = new System.Drawing.Point(834, 398);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(201, 30);
            this.descriptionTextBox.TabIndex = 23;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 680);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Lab6";
            this.tabControl1.ResumeLayout(false);
            this.clientsTypesPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.clientTypesGridView)).EndInit();
            this.cleanersPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cleanersGridView)).EndInit();
            this.estateTypesTab.ResumeLayout(false);
            this.estateTypesTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.estateTypesGridView)).EndInit();
            this.estateObjectsPage.ResumeLayout(false);
            this.estateObjectsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.estateObjectsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage clientsTypesPage;
        private System.Windows.Forms.TabPage cleanersPage;
        private System.Windows.Forms.DataGridView clientTypesGridView;
        private System.Windows.Forms.Button saveClientTypeButton;
        private System.Windows.Forms.Button deleteClientTypeButton;
        private System.Windows.Forms.Button addClientTypeButton;
        private System.Windows.Forms.DataGridView cleanersGridView;
        private System.Windows.Forms.Button saveCleanerButton;
        private System.Windows.Forms.Button deleteCleanerButton;
        private System.Windows.Forms.Button addCleanerButton;
        private System.Windows.Forms.TabPage estateTypesTab;
        private System.Windows.Forms.Button updateEstateType;
        private System.Windows.Forms.Button deleteEstateType;
        private System.Windows.Forms.Button addEstateType;
        private System.Windows.Forms.DataGridView estateTypesGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox typeDescTextBox;
        private System.Windows.Forms.TextBox typeNameTextBox;
        private System.Windows.Forms.TabPage estateObjectsPage;
        private System.Windows.Forms.DataGridView estateObjectsGridView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox houseNumberTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox streetTextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox objectTypeComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox daysDurationTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox sumTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ownerEmailComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox squareTextBox;
    }
}


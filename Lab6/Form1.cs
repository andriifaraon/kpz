﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public partial class Form1 : Form
    {
        DataSet clientTypesDataSet;
        DataSet cleanersDataSet;
        EstateTypesContext estateTypesContext;
        EstateObjectContext estateObjectContext;
        readonly string connectionString = @"Data Source=.\SQLEXPRESS01;Initial Catalog=EstateRentDB;Integrated Security=True";
        public Form1()
        {
            InitializeComponent();
            DataManipulations.LoadData(clientTypesGridView, connectionString, "SELECT * FROM ClientsTypes", out clientTypesDataSet);
            DataManipulations.LoadData(cleanersGridView, connectionString, "SELECT * FROM Cleaners", out cleanersDataSet);
            ComboBoxSetter.LoadDataToCombobox(ownerEmailComboBox, connectionString, "SELECT * FROM Clients", "ClientEmail", "ClientEmail");
            ComboBoxSetter.LoadDataToCombobox(objectTypeComboBox, connectionString, "SELECT * FROM EstateTypes", "TypeName", "TypeID");
            estateTypesContext = new EstateTypesContext();
            estateTypesContext.EstateTypes.Load();
            estateTypesGridView.DataSource = estateTypesContext.EstateTypes.Local.ToBindingList();
            estateTypesGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            estateTypesGridView.Columns[0].Visible = false;

            estateObjectContext = new EstateObjectContext();
            estateObjectContext.EstateObjects.Load();
            estateObjectsGridView.DataSource = estateObjectContext.EstateObjects.Local.ToBindingList();
            estateObjectsGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            estateObjectsGridView.Columns[0].Visible = false;
        }

        #region ClientsTypes
        private void SaveClientTypeButton_Click(object sender, EventArgs e)
        {
            DataManipulations.SaveData( connectionString, "SELECT * FROM ClientsTypes", ref clientTypesDataSet);
        }

        private void DeleteClientTypeButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in clientTypesGridView.SelectedRows)
            {
                clientTypesGridView.Rows.Remove(row);
            }
        }

        private void AddClientTypeButton_Click(object sender, EventArgs e)
        {
            DataRow row = clientTypesDataSet.Tables[0].NewRow(); 
            clientTypesDataSet.Tables[0].Rows.Add(row);
        }
        #endregion

        #region Cleaners
        private void SaveCleanerButton_Click(object sender, EventArgs e)
        {
            DataManipulations.SaveData(connectionString, "SELECT * FROM Cleaners", ref cleanersDataSet);
        }

        private void DeleteCleanerButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in cleanersGridView.SelectedRows)
            {
                cleanersGridView.Rows.Remove(row);
            }
        }

        private void AddCleanerButton_Click(object sender, EventArgs e)
        {
            DataRow row = cleanersDataSet.Tables[0].NewRow();
            cleanersDataSet.Tables[0].Rows.Add(row);
        }
        #endregion

        #region EstateTypes
        private void AddEstateType_Click(object sender, EventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            EstateType estateType = new EstateType();
            estateType.TypeName = typeNameTextBox.Text;
            estateType.TypeDescription = typeDescTextBox.Text;

            estateTypesContext.EstateTypes.Add(estateType);
            estateTypesContext.SaveChanges();
            stopwatch.Stop();
            MessageBox.Show("Executing ADO.NET EF Adding: " + stopwatch.ElapsedMilliseconds);
        }
        private void DeleteEstateType_Click(object sender, EventArgs e)
        {
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                if (estateTypesGridView.SelectedRows.Count > 0)
                {
                    int index = estateTypesGridView.SelectedRows[0].Index;
                    bool converted = int.TryParse(estateTypesGridView[0, index].Value.ToString(), out int id);
                    if (converted == false)
                        return;

                    EstateType player = estateTypesContext.EstateTypes.Find(id);
                    estateTypesContext.EstateTypes.Remove(player);
                    estateTypesContext.SaveChanges();
                }
                stopwatch.Stop();
                MessageBox.Show("Executing ADO.NET EF Deleting: " + stopwatch.ElapsedMilliseconds);
            }
            catch(Exception exeption)
            {
                MessageBox.Show(exeption.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void UpdateEstateType_Click(object sender, EventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            if (estateTypesGridView.SelectedRows.Count > 0)
            {      
                estateTypesContext.SaveChanges();
                estateTypesGridView.Refresh();
            }
            stopwatch.Stop();
            MessageBox.Show("Executing ADO.NET EF Updating: " + stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region EstateObjects

        #endregion

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (estateObjectsGridView.SelectedRows.Count > 0)
                {
                    int index = estateObjectsGridView.SelectedRows[0].Index;
                    bool converted = int.TryParse(estateObjectsGridView[0, index].Value.ToString(), out int id);
                    if (converted == false)
                        return;

                    EstateObject estateObject = estateObjectContext.EstateObjects.Find(id);
                    estateObjectContext.EstateObjects.Remove(estateObject);
                    estateObjectContext.SaveChanges();
                }
            }
            catch (Exception exeption)
            {
                MessageBox.Show(exeption.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (estateObjectsGridView.SelectedRows.Count > 0)
            {
                estateObjectContext.SaveChanges();
                estateObjectsGridView.Refresh();
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            EstateObject estateObject = new EstateObject();
            estateObject.City = cityTextBox.Text;
            estateObject.Street = streetTextBox.Text;
            estateObject.HouseNumber = int.Parse(houseNumberTextBox.Text);
            estateObject.ObjectType = int.Parse(objectTypeComboBox.SelectedValue.ToString());
            estateObject.Square = decimal.Parse(squareTextBox.Text);
            estateObject.OwnerEmail = ownerEmailComboBox.SelectedValue.ToString();
            estateObject.Description = descriptionTextBox.Text;
            estateObject.Sum = decimal.Parse(sumTextBox.Text);
            estateObject.DaysDuration = int.Parse(daysDurationTextBox.Text);
            estateObjectContext.EstateObjects.Add(estateObject);
            estateTypesContext.SaveChanges();
        }

    }
}

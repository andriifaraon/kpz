﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;

namespace Lab6
{
    public static class DataManipulations
    {
        public static void LoadData(DataGridView dataGridView, string connectionString, string sqlCommand, out DataSet dataSet)
        {
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.AllowUserToAddRows = false;
            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, connection);
                dataSet = new DataSet();
                adapter.Fill(dataSet);
                dataGridView.DataSource = dataSet.Tables[0];
                dataGridView.Columns[0].Visible = false;
            }
        }
        public static void SaveData(string connectionString, string sqlCommand, ref DataSet dataSet)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, connection);
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);

                    adapter.Update(dataSet);
                }
                catch( SqlException ex )
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            stopwatch.Stop();
            MessageBox.Show("Executing ADO.NET: " + stopwatch.ElapsedMilliseconds);
        }
    }
}

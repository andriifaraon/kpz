﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Lab6
{
    public static class ComboBoxSetter
    {
        public static void LoadDataToCombobox(ComboBox comboBox, string connectionString, string sqlCommand, string member, string valueMember)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand, connection);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                comboBox.DataSource = dataSet.Tables[0];
                comboBox.DisplayMember = member;
                comboBox.ValueMember = valueMember;
            }
        }
    }
}

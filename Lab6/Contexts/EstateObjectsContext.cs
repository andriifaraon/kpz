﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Lab6
{
    public class EstateObjectContext : DbContext
    {
        public EstateObjectContext() : base("DefaultConnection")
        { }

        public DbSet<EstateObject> EstateObjects { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Lab6
{
    public class EstateTypesContext: DbContext
    {
        public EstateTypesContext() : base("DefaultConnection")
        {
            
        }

        public DbSet<EstateType> EstateTypes { get; set; }
    }
}
